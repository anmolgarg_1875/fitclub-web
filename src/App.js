import React, { Component, Suspense, Fragment } from 'react';
import { Route, Switch }              from 'react-router-dom';
import Spinner                        from './components/UI/Loader/Loader';
 
import Layout                         from './hoc/Layout/Layout';
import ProtectedRoute                 from './services/ProtectedRoute';
import Home                           from '../src/containers/Home/Home';
import Login                          from '../src/containers/Login/Login';
import SignUp                         from '../src/containers/SignUp/SignUp';

// ============ Code Splitting [Route Lazyload] ==============
const SignUpSteps      = React.lazy( () => import('./containers/SignUpSteps/SignUpSteps'));
const InternalLayout   = React.lazy( () => import('./hoc/InternalLayout/InternalLayout'));
// ===========================================================

class App extends Component {
  render() {
    return (
      <Fragment>

        <Suspense fallback={<div>Loading...</div>}>
          <Layout>
            <Switch>

              <Route path = "/Home"        exact  component = { Home } />
              <Route path = "/Login"       exact  component = { Login } />
              <Route path = "/SignUpSteps"        component = { SignUpSteps } />
              <Route path = "/SignUp"      exact  component = { SignUp } />

              {/* ================Redirect to dashboard==================== */}

              <ProtectedRoute path = "/"          component = { InternalLayout } />

              {/* ==========================  =============================== */}

              <Route path = "*"            exact  component = { ()=>(<h1>404 Not Fount !</h1>) } />  

            </Switch>
          </Layout>
        </Suspense>

        {/* ==========PAGE LOADER============== */}

        <Spinner />

        {/* =================================== */}

      </Fragment>
    );
  }
}

export default App;
