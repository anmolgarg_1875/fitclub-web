import React ,{ Component } from 'react';
import { Route, Redirect } from 'react-router-dom';

import Header from '../../components/Header/Header';
import Dashboard from '../../containers/Dashboard/Dashboard';


class InternalLayout extends Component {

    componentDidMount() {
        if (this.props.history.location.pathname === "/") {
            this.props.history.push('/Dashboard');
        } else {
            this.props.history.push(this.props.history.location.pathname);
        }
    }

    render() {
        return(
            <div>
                <Header {...this.props}/>
                <Route path="/Dashboard" component = { Dashboard }  />
            </div>
        );
    }

}

export default InternalLayout;