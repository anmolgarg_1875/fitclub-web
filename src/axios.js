import axios from 'axios';
import messageService from './services/subscriberService';

const instance = axios.create({
    baseURL : "https://dev.api.bambooapp.com/"
});

instance.interceptors.request.use(request => {
    messageService.sendMessage(true);
    let token = localStorage.getItem('accessToken');
    if (token)  {
        request.headers.authorization = 'Bearer '+token;
    }
    return request ;
});

instance.interceptors.response.use(response => {
    messageService.sendMessage(false);
    return response ;
});

instance.defaults.headers.common['content-language'] = 'en';

export default instance;