import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
// ===============PRIME REACT CSS===============
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
// =============================================
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

require("bootstrap/dist/css/bootstrap.min.css");
require("react-datepicker/dist/react-datepicker-cssmodules.css");
require("react-loader-spinner/dist/loader/css/react-spinner-loader.css");

const app = (
    <BrowserRouter>
        <App />
    </BrowserRouter>
);

ReactDOM.render(app , document.getElementById('root'));
registerServiceWorker();
