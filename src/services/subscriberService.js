import { Subject } from 'rxjs';

const subject = new Subject();

const messageService = {
    sendMessage: message => subject.next({ show: message }),
    getMessage: () => subject.asObservable()
}

export default messageService;