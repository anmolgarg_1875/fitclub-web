const ApiURL = {
    _login: 'api/v1/fitclub/coach-login',    
    _signUp: 'api/v1/fitclub/coach-signup',    
    _getUsers: 'api/v1/fitclub/users',    
    _getSignUpData: 'api/v1/fitclub/access-token-login',    
    _updateProfile: 'api/v1/fitclub/coach-update-profile',    
    _addActivity: 'api/v1/fitclub/add-activity',    
    _getActivity: 'api/v1/fitclub/get-activities',   
    _getActivityTypes: 'api/v1/fitclub/get-activity-types'   
}

export default ApiURL;