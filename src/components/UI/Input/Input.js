import React from 'react';
import classes from './Input.css';

const input = (props) => {
    let inputElement = null;

    switch(props.elementType) {
        case ( 'input' ):
            inputElement = <input {...props.elementConfig} value={props.value} />;
            break;
        case ( 'textarea' ):
            inputElement = <textarea {...props.elementConfig} value={props.value} />;
            break;
        default:
            inputElement = <input />;
    }  
    
    return (
        <div className={classes.input_div}>
            <label>{props.title}</label>    
            {inputElement}
        </div>
    );
};

export default input;

