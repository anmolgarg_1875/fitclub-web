import React from  'react';

const SelectBox = ({ handler, touched, hasError, meta }) => (
    <div>
        {meta.label !== ""?<label>{meta.label}</label>:null}
        <select {...handler()}>
            <option value="" disabled> Select </option>
            {meta.value.map((el,index) => <option key={el+index} value={el.value}>{el.label}</option>)}
        </select>
        <span>
            {touched && hasError("required") && `${meta.label} is required`}
        </span>
    </div>
);

export default SelectBox;