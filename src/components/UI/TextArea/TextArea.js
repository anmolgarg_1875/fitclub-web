import React from 'react';

const TextArea = ({ handler, touched, hasError, meta }) => (
    <div>
        <label>{meta.label}</label>
        <textarea rows={meta.rows?meta.rows:4} cols={meta.cols?meta.cols:50}
         maxLength={meta.maxLength?meta.maxLength:''}  placeholder={`Enter ${meta.label}`} {...handler()}/>
        <span>
            {touched && hasError("required") && `${meta.label} is required`}
        </span>
        <span>
            {touched && hasError("minLength") && `min length should be ${meta.minLength}`}
        </span>
        <span>
            {touched && hasError("maxLength") && `max length should be ${meta.maxLength}`}
        </span>
    </div>  
  )

export default TextArea;