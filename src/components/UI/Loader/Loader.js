import  React, {Component} from 'react';
import Loader from 'react-loader-spinner';
import messageService from '../../../services/subscriberService';

import classes from './Loader.css';

class Spinner extends Component {
    state = {
        show: false
    }

    componentDidMount() {
        this.subscription = messageService.getMessage().subscribe(data => {
            if (data) { 
                this.setState({show: data.show});
            } 
        });
    }
    
    render() {
        return (
                <Loader className = {classes.loader}
                    visible={this.state.show}
                    type="ThreeDots"
                    color="#F22B5D"
                    height={100}
                    width={100}
                />
        );
    }
}


export default Spinner;