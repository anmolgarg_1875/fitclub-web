import React, {Component} from 'react';
import {Modal} from 'react-bootstrap';

import classes from './BootstrapModal.css'

class BootstrapModal extends Component {

    render () {
        return (
            <Modal
                size="md"
                {...this.props}
                aria-labelledby="contained-modal-title-vcenter"
                centered
                >
                <Modal.Header closeButton  className={classes.modal_heading}>
                    <Modal.Title id="contained-modal-title-vcenter">
                        <p>{this.props.title}</p>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className={classes.modal_content}>
                    <div>
                        {this.props.children}
                    </div>
                </Modal.Body>
                {/* <Modal.Footer  className={classes.modal_content}>
                    <Button onClick={this.props.onHide}>Close</Button>
                </Modal.Footer> */}
            </Modal>
          );
    }      
}

export default BootstrapModal;