import React from 'react';

const TextInput = ({ handler, touched, hasError, meta }) => (
    <div>
        {meta.label !== ""?<label>{meta.label}</label>:null}
        <input type={meta.type} rows="4" cols="50" maxLength={meta.maxLength?meta.maxLength:''}  placeholder={`${meta.label}`} {...handler()}/>
        <span>
            {touched && hasError("required") && `${meta.label} is required`}
        </span>
        <span>
            {touched && hasError("minLength") && `min length should be ${meta.minLength}`}
        </span>
        <span>
            {touched && hasError("maxLength") && `max length should be ${meta.maxLength}`}
        </span>
        <span>
            {touched && hasError("pattern") && `Invalid ${meta.label}`}
            {/* {meta.label === 'Password' && touched && hasError("pattern") && `Invalid ${meta.label}`} */}
        </span>
    </div>  
  )

export default TextInput;