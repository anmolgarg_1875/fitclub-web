import React from 'react';

import classes from './Header.css';;
 
const header = (props) => {

    const logout = () => {
        localStorage.removeItem('accessToken');
        localStorage.removeItem("formStep");
        localStorage.removeItem("gymId");
        props.history.push({pathname: '/Home'});
    }

    return (
        <header>            
            <div className={classes.header_div}>            
                <div className={classes.logo_div}>    
                    <a><span>F</span>IT<span>C</span>LUB</a>    
                </div>
                <nav className={classes.navbar}>        
                    <a onClick={logout}>Logout</a>
                </nav>
            </div>
        </header>
    );
}

export default header;
