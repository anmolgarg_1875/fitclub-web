import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { FormBuilder, FieldGroup, FieldControl, Validators } from "react-reactive-form";
import axios from '../../axios';
import ApiURL from '../../services/ApiURL';

import classes from './SignUp.css';
import peopleImage from '../../assets/Images/yoga.jpg';
import TextInput from '../../components/UI/FormInput/FormInput';


class SignUp extends Component {

    state = {
        date: new Date()
    }

    loginForm = FormBuilder.group({
        username : ["", [Validators.required,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$')]],
        password : ["", [Validators.required,Validators.minLength(10),Validators.maxLength(50)]]
    });

    handleSubmit=(e) => {
        e.preventDefault();
        if (this.loginForm.valid) {
            const formData = {
                'email': this.loginForm.value.username,
                'password': this.loginForm.value.password
            }
            axios.post(ApiURL._signUp, formData).then(response => {
                // ToastsStore.success("Sign up successfull!", 2000);
                localStorage.setItem("formStep", 1);
                localStorage.setItem('accessToken', response.data.data.accessToken);
                this.props.history.push({
                    pathname: '/SignUpSteps'
                });
            });
        }
    }

    render() {
        console.log("this.state", this.state);
        return (
            <section>
                <div className = {classes.banner_image}>    
                </div>
                <div className={classes.login_container}>    
                    <div className={classes.login_gym_image}>         
                    <img src = {peopleImage}  alt="people"/>      
                    </div>
                    <div className={classes.login_form}>
                    <h1>Create a FitClub account</h1>
                        <p>If you create an account with Facebook, you’ll get free access to workout content</p>
                        <button className={classes.fb_button}>Continue with facebook</button>
                         <FieldGroup control={this.loginForm}
                            render={({ get, invalid }) => (
                                <form onSubmit={this.handleSubmit}>
                                    <FieldControl name="username" render={TextInput} meta={{ label: "Email", type: "text" }} />
                                    <FieldControl name="password" render={TextInput}
                                     meta={{ label: "Password", type: "password", minLength: '10', maxLength:'50' }} />
                                    <button type="submit"  className={invalid?classes.Disabled_Button: classes.Enabled_Button}
                                     disabled={invalid} >Submit</button>
                                </form>
                             )}
                        />
                        <p style={{marginTop: '20px'}}>Already have an account?  <Link to="/Login">Login</Link> instead.</p>
                    </div>
                </div>
            </section>
        );
    }

};

export default SignUp;