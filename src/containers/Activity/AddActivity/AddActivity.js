import React, {Component} from 'react';
import{FormBuilder, FieldGroup, FieldControl,
Validators, FieldArray} from 'react-reactive-form';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes, faPlus } from '@fortawesome/free-solid-svg-icons'
import {Calendar} from 'primereact/calendar';
import axios from '../../../axios';
import ApiURL from '../../../services/ApiURL';

import classes from './AddActivity.css';
import TextInput from '../../../components/UI/FormInput/FormInput';
import SelectBox from '../../../components/UI/SelectBox/SelectBox';

class AddActivity extends Component {

    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            coaches: this.props.coaches.length?this.props.coaches:[],
            activityTypes: this.props.activityTypes.length?this.props.activityTypes:[],
            equipmentsOptions: [
                {label:'Optional', value: 'false'},
                {label:'Mandatory', value: 'true'},
            ],
            duration: [
                {label:'15 min', value: '15'},
                {label:'30 min', value: '30'},
                {label:'45 min', value: '45'},
                {label:'60 min', value: '60'},
            ],
            intensityLevel: [
                {label:'Open', value: 'OPEN'},
                {label:'Intermediate', value: 'INTERMEDIATE'},
                {label:'Advanced', value: 'ADVANCE'},
            ]
        };
    }

    activityForm = FormBuilder.group({
        class: ['', Validators.required],
        activityType: ['', Validators.required],
        date: [new Date(), Validators.required],
        duration: ['30', Validators.required],
        coachName: ['', Validators.required],
        intensityLevel: ['OPEN', Validators.required],
        equipments: FormBuilder.array([])
    });

    componentDidMount() {
        console.log("Add Activity", this.props);
        this.addItem();
    }

    // Adds an item in Form Array
    addItem = () =>  {
        const itemsControl = this.activityForm.get("equipments");
        itemsControl.push(this.createItem());
    }
    // Removes an item
    removeItem = (index) => {
        const itemsControl = this.activityForm.get("equipments");
        itemsControl.removeAt(index);
    }

    // Creates the unique keys
    getKey = () => {
        return this.keyCount++;
    };

    createItem = () => {
        const control = FormBuilder.group({
          name: ["", Validators.required],
          required: "false"
        });
        // Adding key
        control.meta = {
          key: this.getKey()
        };
        return control;
      }

    dateChangeHandler = (event) => {
        this.activityForm.patchValue({
            date: event.target.value
        });
        this.activityForm.markAsDirty();
      };

    handleSubmit=(e) => {
        e.preventDefault();
        console.log(this.activityForm.valid, this.activityForm.value);
        if (this.activityForm.valid) {
            const formData = new FormData();
            formData.append('typeId', this.activityForm.value.activityType);
            formData.append('name', this.activityForm.value.class);
            formData.append('duration', this.activityForm.value.duration);
            formData.append('startTime', this.activityForm.value.date);
            formData.append('coachId', this.activityForm.value.coachName);
            formData.append('intensityLevel', this.activityForm.value.intensityLevel);
            formData.append('gymId', localStorage.getItem('gymId'));
            formData.append('equipments', JSON.stringify(this.activityForm.value.equipments));
            axios.post(ApiURL._addActivity, formData).then(response => {
                console.log("response", response);    
                this.props.updateActivity();
                this.props.onHide();
            })
        }
    }

       render() {
           console.log("this.state.coaches", this.state.coaches);
        return (
            <div className = {classes.login_form}> 
                <FieldGroup control = {this.activityForm} render={({get, invalid}) => (
                    <form onSubmit={this.handleSubmit}>
                        <div className = {classes.form_group_container}>
                            <div className = {classes.form_group}> 
                                <FieldControl name="class" 
                                render={TextInput} meta={{ label: "Class Name", type: "text", maxLength:30 }} />
                            </div>
                            <div className = {classes.form_group}> 
                                <FieldControl name="activityType" 
                                render={SelectBox} meta={{ value: this.state.activityTypes, label: "Activity Type"}}/>
                            </div>
                        </div>
                        <div className = {classes.form_group_container}>
                            <div className = {classes.form_group}> 
                                <label>Start Time</label>
                                <Calendar dateFormat="MM/dd/yy" value = {this.state.date}  onChange={this.dateChangeHandler}></Calendar>
                            </div>
                            <div className = {classes.form_group}> 
                                <FieldControl name="duration" 
                                render={SelectBox} meta={{ value: this.state.duration, label: "Duration"}}/>
                            </div>
                        </div>
                        <div className = {classes.form_group_container}>
                            <div className = {classes.form_group}> 
                                <FieldControl name="coachName" 
                                render={SelectBox} meta={{ value: this.state.coaches, label: "Coach Name"}}/>
                            </div>
                            <div className = {classes.form_group}> 
                                <FieldControl name="intensityLevel" 
                                render={SelectBox} meta={{ value: this.state.intensityLevel, label: "Intensity Level"}}/>
                            </div>
                        </div>

                        {/* ===========   Equipments Array    ============== */}
                        <div className = {classes.fieldArray_container}>
                            <label className={classes.equipmenty_label}>Equipments</label>
                            <FieldArray
                                name="equipments"
                                render={({ controls }) => (
                                <div>
                                    <div>
                                        {/* <span onClick={() => this.addItem()} className = {classes.add_equipment}><FontAwesomeIcon icon = { faPlus } />Add Equipment</span> */}
                                    </div>
                                    {controls.map((productControl, index) => (
                                        <div key={`${productControl.meta.key}-${String(index)}`}>
                                            <FieldGroup
                                                control={productControl}
                                                render={() => (
                                                    <div className = {classes.fieldArray_container}>
                                                        <div className = {classes.form_group}> 
                                                            <FieldControl name="name" 
                                                            render={TextInput} 
                                                            meta={{ label: "", type: "text", maxLength:8 }} />
                                                        </div>
                                                        <div className = {classes.form_group}> 
                                                            <FieldControl name="required" render={SelectBox}
                                                            meta={{ value: this.state.equipmentsOptions }}/>
                                                        </div>    
                                                        <span className = {classes.remove_equipment}>
                                                            <FontAwesomeIcon onClick={() => this.removeItem(index)} icon = { faTimes } />
                                                        </span>
                                                    </div>
                                            )} />
                                        </div>
                                    ))}
                                      <span onClick={() => this.addItem()} className = {classes.add_equipment}><FontAwesomeIcon icon = { faPlus } />Add Equipment</span>
                                </div>
                            )} />
                        </div>

                        {/* ===========   Equipments Array End    ============== */}

                        <button type="submit" className={invalid?classes.Disabled_Button: classes.Enabled_Button}
                        disabled={invalid} >Submit</button>
                    </form>
                )} 
                />
            </div>
        );
    }
}

export default AddActivity