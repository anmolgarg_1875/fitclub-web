import React from 'react';
import classes from './GenderRadio.css';

const genderRadio = ({ handler }) => (
    <div>
      <div>
        <label>Gender:</label>
      </div>
      <div className = {classes.radio_box}>
        <div className = {classes.radio_box_item}>
            <label>Male</label>
            <input {...handler("radio", "MALE")} />
        </div>
        <div className = {classes.radio_box_item}>
            <label>Female</label>
            <input {...handler("radio", "FEMALE")} />
        </div>
        <div className = {classes.radio_box_item}>
            <label>Other</label>
            <input {...handler("radio", "OTHER")} />
        </div>
      </div>
    </div>
  );

  export default genderRadio;