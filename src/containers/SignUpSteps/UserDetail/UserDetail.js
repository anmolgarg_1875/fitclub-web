import React, { Component } from 'react';
import { Field, FormBuilder, FieldGroup, FieldControl, Validators } from "react-reactive-form";
import {Calendar} from 'primereact/calendar';
import axios from '../../../axios';
import ApiURL from '../../../services/ApiURL';

import TextInput from '../../../components/UI/FormInput/FormInput';
import GenderRadio from './GenderRadio/GenderRadio';
import classes from './UserDetail.css';
import userDummy from '../../../assets/Images/dummy.jpg';

//  =============  SIGN UP STEP [0]==================   

class UserDetail extends Component {

    state = {
        date: new Date(),
        image: "",
        caochProfileStep: 0
    };

    userDetailForm = FormBuilder.group({
        profilePicFile :   ["", Validators.required],
        firstName :   ["", Validators.required],
        lastName :    ["", Validators.required],
        dob : ['', Validators.required],
        gender : ["MALE", Validators.required]
    });

    componentDidMount() {
        // this.props.history.push({
        //     pathname: '/SignUpSteps',
        //     search: '?step=userdetail'
        // });
        console.log("User Detail", this.props);    
        this.updateStateHandler(0);    
    }
    
    componentDidUpdate() {
        console.log(this.props);
        console.log(typeof this.props.data.profilePicFile);
        if (this.props.data.profilePicFile!== "" &&
            this.props.data.profilePicFile !== this.state.image && 
            this.state.image === "") {
            this.updateStateHandler(1);    
        }
    }

    // ============update state handler============

    updateStateHandler = (updateType) => {
        this.userDetailForm.patchValue({
            firstName: this.props.data.firstName,
            lastName: this.props.data.lastName,
            dob: this.props.data.dob,
            gender: this.props.data.gender,
            profilePicFile: this.props.data.profilePicFile
        });
        if( updateType ) {
            this.setState({
                image: this.props.data.profilePicFile,
                date: this.props.data.dob
            });
        } else {
            if (this.userDetailForm.value.profilePicFile !== "") {
                this.setState({image: this.props.data.profilePicFile});
            }
            if (this.userDetailForm.value.dob !== "") {
                this.setState({date: this.userDetailForm.value.dob});
            }
        }
    }

    //  ======patching of date value========

       dateChangeHandler = (event) => {
        this.userDetailForm.patchValue({
            dob: event.target.value
        });
        this.userDetailForm.markAsDirty();
      };

    //  ======patching of Image value========

    imageChangeHandler = (event) =>  {
        console.log("Image ", event.target.files[0]);
        this.setState({image: URL.createObjectURL(event.target.files[0])});
        this.userDetailForm.patchValue({
            profilePicFile: event.target.files[0]
        });
        this.userDetailForm.markAsDirty();
        console.log(typeof this.userDetailForm.value.profilePicFile);
    }

    // ===============form submit handler===============

    handleSubmit=(e) => {
        e.preventDefault();
        // ======================To get date string=================
        const obj = {
            'date':  new Date(this.userDetailForm.value.dob).getDate() , 
            'month':  new Date(this.userDetailForm.value.dob).getMonth(),  
            'year':  new Date(this.userDetailForm.value.dob).getFullYear()  
        }
        //  ========  if form  is valid and any field value changed =========
        if (this.userDetailForm.valid && this.userDetailForm.dirty) {

            const formData = new FormData();
            formData.append('firstName', this.userDetailForm.value.firstName);
            formData.append('lastName', this.userDetailForm.value.lastName);
            formData.append('dob', JSON.stringify(obj));
            formData.append('gender', this.userDetailForm.value.gender);
            if (this.state.caochProfileStep >= this.props.data.caochProfileStep) {
                formData.append('caochProfileStep', 0);
            }
            if (typeof this.userDetailForm.value.profilePicFile === "object") {
                formData.append('profilePicFile', this.userDetailForm.value.profilePicFile);
            }

            axios.post( ApiURL._updateProfile, formData ).then( response => {
                this.userDetailForm.patchValue({
                    profilePicFile: response.data.data.profilePic.thumb,        
                    dob: new Date(response.data.data.dob.year, response.data.data.dob.month, response.data.data.dob.date )
                }); 
                this.props.updateState({...this.userDetailForm.value,caochProfileStep: response.data.data.caochProfileStep}, 0);
                this.props.updateForm();                      
            });

        } else if (this.userDetailForm.valid && this.userDetailForm.pristine) {
                this.props.updateForm();                      
        }   
    }

    render() {
        return(
                <div className = {classes.login_container}>
                    <div className = {classes.login_form}>
                        <h3>Tell us about yourself</h3>
                        <FieldGroup control={this.userDetailForm}
                            render={({ get, invalid }) => (
                                <form onSubmit={this.handleSubmit}>
                                    <div className={classes.vendor_left_menu}>
                                    <div className={classes.vendor_left_menu_img}>
                                        <div className={classes.vendor_left_menu_img_upload}>
                                            <Field control={this.userDetailForm.get('profilePicFile')}  render={({ onChange, value }) => (
                                                <input type="file" onChange={this.imageChangeHandler} accept="image/*" />    
                                            )}
                                        />
                                        </div>
                                        <img src = {this.state.image === ''?userDummy:this.state.image} alt="profile" />
                                    </div>
                                    <p>Add Profile Photo</p>
                                    </div>
                                    <FieldControl name="firstName" render={TextInput} meta={{ label: "First Name", type: "text" }} />
                                    <FieldControl name="lastName" render={TextInput} meta={{ label: "Last Name", type: "text" }} />
                                    <label>Date Of Birth</label>
                                    <Calendar maxDate={new Date()} dateFormat="MM/dd/yy" value = {this.state.date}  onChange={this.dateChangeHandler}></Calendar>
                                    <FieldControl name="gender" render={GenderRadio} />
                                    <button type="submit" className={invalid?classes.Disabled_Button: classes.Enabled_Button}
                                     disabled={invalid} >Continue</button>
                                </form>
                            )}
                        />  
                    </div>
            </div>
        );
    }        
};

export default UserDetail;