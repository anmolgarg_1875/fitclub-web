import React, {Component} from 'react';
import { FormBuilder, FieldGroup, FieldControl, Validators } from "react-reactive-form";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons'
import axios from '../../../axios';
import ApiURL from '../../../services/ApiURL';

import TextInput from '../../../components/UI/FormInput/FormInput';
import classes from './Membership.css'

//  =============  SIGN UP STEP [2]==================   

class Membership extends Component {

    state = {
        caochProfileStep: 2,
        membershipCode: ""
    }

    membershipForm = FormBuilder.group({
        membershipCode: ["", [Validators.required,Validators.pattern('^[a-zA-Z]{4}[0-9]{4}')]]
    });

    componentDidMount() {
        console.log("Membership", this.props);    
        this.membershipForm.patchValue({
            membershipCode: this.props.data.membershipCode                            
        });
        this.setState({membershipCode: this.props.data.membershipCode });
    }

    componentDidUpdate() {
        console.log("Membership", this.props);    
        if (this.state.membershipCode === "" && this.props.data.membershipCode !== "") {
            this.setState({membershipCode: this.props.data.membershipCode });
            this.membershipForm.patchValue({
                membershipCode: this.props.data.membershipCode                            
            });
        }
    }

    // ===============form submit handler===============

    handleSubmit=(e) => {
        e.preventDefault();
            //  ========  if form  is valid and any field value changed =========
        if (this.membershipForm.valid && this.membershipForm.dirty) {
            const formData = new FormData();
            formData.append('membershipCode', this.membershipForm.value.membershipCode);
            if (this.state.caochProfileStep >= this.props.data.caochProfileStep) {
                formData.append('caochProfileStep', 2);
            }

            axios.post(ApiURL._updateProfile, formData).then(response => {
                this.props.updateState({...this.membershipForm.value, caochProfileStep: response.data.data.caochProfileStep}, 2);
                if (response.data.data.gymsLinked && response.data.data.gymsLinked.length) {
                    this.props.updateState({gymData: [...response.data.data.gymsLinked], caochProfileStep: response.data.data.caochProfileStep}, 3);
                }
                this.props.updateForm();
            })
            
        } else if (this.membershipForm.valid && this.membershipForm.pristine) {
            this.props.updateForm();                   
        }
    }

    render() {
        return(
            <div className = {classes.login_container}>
                <div className = {classes.login_form}>
                    <FontAwesomeIcon onClick={this.props.backward} icon = { faArrowCircleLeft } />
                    <h1>Link your gym or fitness studio</h1>
                    <FieldGroup
                        control={this.membershipForm}
                            render={({ get, invalid }) => (
                                <form onSubmit={this.handleSubmit}>
                                    <label>Membership Code</label>
                                    <FieldControl name="membershipCode" 
                                        render={TextInput} meta={{ label: "", type: "text", maxLength:8 }} />
                                    <button type="submit" className={invalid?classes.Disabled_Button: classes.Enabled_Button}
                                    disabled={invalid} >Continue</button>
                                </form>
                            )}
                    />
                </div>
            </div>    
        );
    }        
};

export default Membership;