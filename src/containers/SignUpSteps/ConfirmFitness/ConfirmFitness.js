import React, {Component} from 'react';
import axios from '../../../axios';
import ApiURL from '../../../services/ApiURL';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons'

import classes from './ConfirmFitness.css'
import dummy from '../../../assets/Images/dummy.jpg';

//  =============  SIGN UP STEP [3]==================   

class ConfirmFitness extends Component {

    state= {
        caochProfileStep: 3,
        gymLogo: dummy,
        gymAddress: '',
        gymName: ''
    }

    componentDidMount() {
        console.log("Confirm Fitness", this.props);    
        if (this.props.data.confirmFitness.gymData.length) {
            this.setState({
                gymLogo: this.props.data.confirmFitness.gymData[0].id.logo.thumb,   
                gymAddress: this.props.data.confirmFitness.gymData[0].id.address.city,   
                gymName: this.props.data.confirmFitness.gymData[0].id.name   
            });        
        }
    }

    componentDidUpdate() {
        if (this.state.gymName === "" && this.props.data.confirmFitness.gymData.length
        && this.props.data.confirmFitness.gymData[0].id.name) {
            this.setState({
                gymLogo: this.props.data.confirmFitness.gymData[0].id.logo.thumb,   
                gymAddress: this.props.data.confirmFitness.gymData[0].id.address.city,   
                gymName: this.props.data.confirmFitness.gymData[0].id.name   
            });        
        }
    }

    // ===============form submit handler===============

    handleSubmit = () => {
        const formData = new FormData();
        if (this.props.data.role.roles.length && 
        (this.props.data.role.roles.includes("GYM_OWNER") ||
         this.props.data.role.roles.includes("GYM_ADMIN"))) {
            if (this.state.caochProfileStep >= this.props.data.confirmFitness.caochProfileStep) {
                formData.append('caochProfileStep', 10);
            }

            axios.post(ApiURL._updateProfile, formData).then(response => {
                if (response.data.data.gymsLinked && response.data.data.gymsLinked.length) {
                    localStorage.setItem('gymId', response.data.data.gymsLinked[0].id._id);                                
                }
                localStorage.removeItem('formStep');
                this.props.history.push({pathname: '/'});
            })

         } else {
            if (this.state.caochProfileStep >= this.props.data.confirmFitness.caochProfileStep) {
                formData.append('caochProfileStep', 3);
            }
            axios.post(ApiURL._updateProfile, formData).then(response => {
                this.props.updateState({gymData: [...response.data.data.gymsLinked], caochProfileStep: response.data.data.caochProfileStep}, 3);
                this.props.updateForm();
            })
         }

    };

    render() {
        return(
            <div className = {classes.login_container}>
                <div className = {classes.login_form}>
                <FontAwesomeIcon onClick={this.props.backward} icon = { faArrowCircleLeft } />
                    <h1>Link your gym or fitness studio</h1>
                    <div className={classes.vendor_left_menu}>
                        <div className={classes.vendor_left_menu_img}>
                            <img src = {this.state.gymLogo} alt="Gym Logo" />
                        </div>
                    </div>
                    <h3>{this.state.gymName}</h3>
                    <h3>{this.state.gymAddress}</h3>
                    <button className = {classes.Enabled_Button} onClick={this.handleSubmit}>Continue</button>
                    <p className={classes.or_line}>OR</p>
                    <p><span  onClick={this.props.backward}>Link another Gym/Fitness Studio</span></p>
                </div>
            </div>    
        );
    }        
};

export default ConfirmFitness;