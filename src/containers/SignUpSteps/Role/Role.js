import React , {Component} from 'react';
import axios from '../../../axios';
import ApiURL from '../../../services/ApiURL'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons'

import classes from './Role.css';

//  =============  SIGN UP STEP [1]==================   

class Role extends Component {

    state = {
        COACH: {
            Value: 'COACH',
            isChecked: false     
        },
        GYM_OWNER: {
            Value: 'GYM_OWNER',
            isChecked: false     
        },
        GYM_ADMIN: {
            Value: 'GYM_ADMIN',
            isChecked: false     
        },
        isDisable: true,
        caochProfileStep: 1
    }

    componentDidMount() {
        console.log("RoleJs componentDidMount", this.props);    
        if (this.props.data.roles.length) {
            // ======= Update State =======   
            this.updateCheckboxState(1); 
        }

    }

    componentDidUpdate() {
        console.log("RolejS componentDidUpdate", this.props);    
        const array = Object.keys(this.state)
                      .filter(el => this.state[el].isChecked === true);
        if (this.props.data.roles.length && !array.length) {
            // ======= Update State =======   
            this.updateCheckboxState(1); 
        }               
    }

    // ======== Update State ==================

    updateCheckboxState = (updateType) => {
        if (updateType) {
            Object.keys(this.state).forEach(el => {
                if (this.props.data.roles.includes(el)) {
                    this.setState(prevState => ({
                        [el]: {...prevState[el],
                                isChecked: true
                            }        
                    }));
                }    
            })
        }
        setTimeout(()=> {
            let status = true;
            Object.keys(this.state).forEach(el => {
                if (this.state[el].isChecked === true){
                    status = false;
                }
            })
            this.setState({isDisable: status});      
        },100);
    }

    // ======Checkbox Click Event============
        
    checkboxChange=(e) => {
        const checked = e.target.checked;
        Object.keys(this.state).forEach(el => {
            if (el === e.target.value) {
                this.setState(prevState => ({
                    [el]: {...prevState[el],
                            isChecked: checked
                        }        
                }));   
            }
        });
        this.updateCheckboxState(0);
    }

    handleSubmit=(e) => {
        e.preventDefault();
        const array = Object.keys(this.state)
                      .filter(el => this.state[el].isChecked === true);
        if (!this.state.isDisable
            && this.props.data.roles.sort().toString() !== array.sort().toString() ) 
        {
            const formData = new FormData();
            formData.append('roles', JSON.stringify(array));
            if ( this.state.caochProfileStep >= this.props.data.caochProfileStep ) {
                formData.append('caochProfileStep', 1);
            }
            axios.post(ApiURL._updateProfile, formData).then(response => {
                this.props.updateState({role:[...array],caochProfileStep: response.data.data.caochProfileStep },1);
                this.props.updateForm();    
            });
        } else if (!this.state.isDisable && this.props.data.roles.length === array.length) {
            this.props.updateForm();    
        }
    }

    render() {
        console.log("RoleJSrendering------", this.state);
        return(
            <div className = {classes.login_container}>
                <div className = {classes.login_form}>
                    <FontAwesomeIcon onClick={this.props.backward} icon = { faArrowCircleLeft } />
                    <h1>Select your role</h1>
                    <h4>Please select the role that describes your work best with Gyms/Fitness Studios. You may select more than one role.</h4>
                    <form onSubmit={this.handleSubmit}>
                        <div className = {classes.checkbox_div}>
                            <input value="COACH" onChange = {this.checkboxChange} checked={this.state.COACH.isChecked} type="checkbox" />
                            <p>Coach/Trainer</p>
                            <h4>You have experience as a fitness professional </h4>
                        </div>
                        <div className = {classes.checkbox_div}>
                            <input value="GYM_OWNER" onChange = {this.checkboxChange} checked={this.state.GYM_OWNER.isChecked} type="checkbox" />
                            <p>Gym/Fitness Studio Owner</p>
                            <h4>You own a Gym/Fitness Studio or a franchise for the same </h4>
                        </div>
                        <div className = {classes.checkbox_div}>
                            <input value="GYM_ADMIN" onChange = {this.checkboxChange} checked={this.state.GYM_ADMIN.isChecked} type="checkbox" />
                            <p>Gym/Fitness Studio Admin</p>
                            <h4>You manage a Gym/Fitness Studio </h4>
                        </div>
                        <button type="submit" disabled={this.state.isDisable} onClick={this.handleSubmit}
                        className={this.state.isDisable?classes.Disabled_Button: classes.Enabled_Button}>
                            Continue
                        </button>
                    </form>
                </div>
            </div>
        );
    }
}

export default Role;