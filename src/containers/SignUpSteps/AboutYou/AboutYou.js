import React, {Component} from 'react';
import { FormBuilder, FieldGroup, FieldControl, Validators } from "react-reactive-form";
import axios from '../../../axios';
import ApiURL from '../../../services/ApiURL';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons'

import TextArea from '../../../components/UI/TextArea/TextArea';
import classes from './AboutYou.css';

//  =============  SIGN UP STEP [5]==================   

class AboutYou extends Component {

    state= {
        caochProfileStep: 5
    }

    aboutYouForm = FormBuilder.group({
        description: ["", Validators.required]
    });

    componentDidMount() {
        console.log("About you", this.props.data)
        this.aboutYouForm.patchValue({
            description: this.props.data.description
        });
    }

    componentDidUpdate() {
        console.log("this.props.data", this.props.data);    
        if (this.props.data.description !== "" && 
        this.aboutYouForm.value.description === "") {
            this.aboutYouForm.patchValue({
                description: this.props.data.description                 
            });
        }
    }

    // ===============form submit handler===============

    handleSubmit=(e) => {
        e.preventDefault();
        if (this.aboutYouForm.valid && this.aboutYouForm.dirty) {
            const formData = new FormData();
            formData.append('description', this.aboutYouForm.value.description)
            if (this.state.caochProfileStep >= this.props.data.caochProfileStep) {
                formData.append('caochProfileStep', 10);
            }
            axios.post(ApiURL._updateProfile, formData).then(response => {
                if (response.data.data.gymsLinked && response.data.data.gymsLinked.length) {
                    localStorage.setItem('gymId', response.data.data.gymsLinked[0].id._id);                                
                }
                localStorage.removeItem('formStep');
                this.props.history.push({pathname: '/'});
            });                    
        } else if (this.aboutYouForm.valid && this.aboutYouForm.pristine) {
            this.props.updateForm();            
        } 
    }

    render() {
        return(
            <div className = {classes.login_container}>
                <div className = {classes.login_form}>
                    <FontAwesomeIcon onClick={this.props.backward} icon = { faArrowCircleLeft } />
                    <h1>What describes you best</h1>
                    <FieldGroup
                        control={this.aboutYouForm}
                        render={({ get, invalid }) => (
                            <form onSubmit={this.handleSubmit}>
                                <FieldControl name="description" 
                                render={TextArea} meta={{ label: "Description", maxLength:500, rows:5, cols: 50 }} />
                                <button type="submit" className={invalid?classes.Disabled_Button: classes.Enabled_Button}>
                                    Continue
                                </button>
                            </form>
                        )}
                    />
                </div>
            </div>    
        );
    }

}

export default AboutYou;