import React, 
{Component, Suspense}   from 'react';
import axios            from '../../axios';
import ApiURL           from '../../services/ApiURL' 
import {Route, Switch } from  'react-router-dom';   
 
import Header           from '../../components/Header/Header';
import classes          from './SignUpSteps.css';

// ============ Component Splitting [Route Lazyload] ==============
const  UserDetail     = React.lazy( () => import('./UserDetail/UserDetail'));
const  Role           = React.lazy( () => import('./Role/Role'));
const  Membership     = React.lazy( () => import('./Membership/Membership'));
const  ConfirmFitness = React.lazy( () => import('../../containers/SignUpSteps/ConfirmFitness/ConfirmFitness'));
const  AboutYou       = React.lazy( () => import('../../containers/SignUpSteps/AboutYou/AboutYou'));
// =====================================================================

class SignUpSteps extends Component {
    constructor() {
        super();
        this.state = {
            userDetail: {
                profilePicFile: '',   
                firstName: '',
                lastName: '',    
                dob: new Date(),    
                gender: 'MALE',    
            },
            role: {
                roles: []
            },
            membership: {
                membershipCode: ''
            },
            confirmFitness: {
                gymData: []         
            },
            description: {
                description: ''
            },
            FormStepStatus: 0
        };
    }

    updateOnReload = (data) => {
        console.log(data);
        this.setState(prevState =>({
            userDetail: {
                ...prevState.userDetail,
                firstName: (data.firstName)?data.firstName:'',
                lastName: data.lastName?data.lastName:'',
                profilePicFile: data.profilePic?data.profilePic.thumb:'',
                gender: data.gender?data.gender:'MALE',
                dob: data.dob?new Date(data.dob.year, data.dob.month, data.dob.date ):new Date(),
                caochProfileStep: data.caochProfileStep
            },
            role: {
                ...prevState.role,
                roles: [...data.roles],
                caochProfileStep: data.caochProfileStep
            },
            membership: {
                membershipCode: (data.gymsLinked && data.gymsLinked.length)?data.gymsLinked[0].id.membershipCode:'',
                caochProfileStep: data.caochProfileStep
            },
            confirmFitness: {
                ...prevState.confirmFitness,
                gymData: (data.gymsLinked && data.gymsLinked.length)?[...data.gymsLinked]:[],
                caochProfileStep: data.caochProfileStep
            },
            description: {
                ...prevState.description,
                description: data.description?data.description: '',
                caochProfileStep: data.caochProfileStep
            },
            FormStepStatus: data.caochProfileStep?data.caochProfileStep:0
        }));
        setTimeout(()=> {
            this.formSwitchHandler(this.state.FormStepStatus);
        },100);
    }


    componentDidMount() {
        console.log("SignUpSteps", this.props);
        axios.post(ApiURL._getSignUpData).then(response => {
            this.updateOnReload(response.data.data);
        });
    }

    formSwitchHandler = (formStep)=> {
        localStorage.setItem("formStep", formStep);
        console.log(this.state);
        let formComponent = null;
            switch( true ) {

                case ( formStep === 0 ) :
                // =====================USER DETAIL FORM========================            
                        this.props.history.push(this.props.match.url+"/userDetail");
                    break;

                case ( formStep === 1 ) :
                // =====================ROLE FORM========================            
                        this.props.history.push(this.props.match.url+"/role");
                    break;

                case ( formStep === 2 ) :
                // =====================MEMBERSHIP FORM========================            
                        this.props.history.push(this.props.match.url+"/membership");
                    break;

                case ( formStep === 3 ) :
                // =====================CONFIRM FITNESS FORM========================            
                        this.props.history.push(this.props.match.url+"/confirmFitness");
                    break;

                case ( formStep === 5 ) :
                // =====================DESCRIPTION FORM========================            
                        this.props.history.push(this.props.match.url+"/description");
                    break;
                    
                default :
                // =====================USER DETAIL FORM========================            
                        this.props.history.push(this.props.match.url+"/userDetail");
            }
        return formComponent;
    }

    updateFormHandler = (FormStep) => {
        this.setState({FormStepStatus: FormStep});
        setTimeout(()=> {
            this.formSwitchHandler(this.state.FormStepStatus);
        },100);
    }

    updateStateHandler = (data, step) => {
        console.log(data, step);
        this.setState(prevState => ({
            userDetail: {
                ...prevState.userDetail,
                caochProfileStep: data.caochProfileStep                
            },
            role: { 
                ...prevState.role,
                caochProfileStep: data.caochProfileStep                        
            },
            membership: { 
                ...prevState.membership,
                caochProfileStep: data.caochProfileStep                                                
            },
            confirmFitness: { 
                ...prevState.confirmFitness,
                caochProfileStep: data.caochProfileStep                        
            }                   
        }));
        if (step === 0) {
            console.log("FormData", data);
            this.setState(prevState => ({
                userDetail: { 
                    ...prevState.userDetail,
                    profilePicFile :  data.profilePicFile,                        
                    firstName :   data.firstName,                        
                    lastName :    data.lastName,                        
                    dob : data.dob,                        
                    gender :      data.gender
                }        
            }));
        }
        if (step === 1) {
            console.log("Roles", data);
            this.setState(prevState => ({
                role: { 
                    ...prevState.role,
                    roles :  [...data.role]                        
                }        
            }));
        }
        if (step === 2) {
            console.log("Membership", data);
            this.setState(prevState => ({
                membership: { 
                    ...prevState.membership,
                    membershipCode :  data.membershipCode                                              
                }        
            }));
        }
        if (step === 3) {
            console.log("Confirm Fitness", data);
            this.setState(prevState => ({
                confirmFitness: { 
                    ...prevState.confirmFitness,
                    gymData :  [...data.gymData]                       
                }        
            }));
        }
        if (step === 5) {
            console.log("Description", data);
            this.setState(prevState => ({
                description: { 
                    ...prevState.description,
                    description :  data.description                        
                }        
            }));
        }

    }

    render() {

        console.log(this.state);
        return(
            <div>
                <Header {...this.props} />
                <section className = {classes.banner_image}>
                    <Switch>
                        <Route path={this.props.match.url+"/userDetail"} 
                        render = { (props) => { return <UserDetail data = { this.state.userDetail } {...props}
                                                updateState = {this.updateStateHandler}
                                                updateForm = {()=>this.updateFormHandler(1)} />}} 
                        />
                        <Route path={this.props.match.url+"/role"} 
                        render = { (props) => { return <Role data = { this.state.role } {...props}
                                                        updateState = {this.updateStateHandler}
                                                        backward = {()=>this.updateFormHandler(0)}
                                                        updateForm = {()=>this.updateFormHandler(2)} />}} 
                        />
                        <Route path={this.props.match.url+"/membership"} 
                        render = { (props) => { return <Membership data = { this.state.membership } {...props}
                                                        updateState = {this.updateStateHandler}
                                                        backward = {()=>this.updateFormHandler(1)}
                                                        updateForm = {()=>this.updateFormHandler(3)} />}} 
                        />
                        <Route path={this.props.match.url+"/confirmFitness"} 
                        render = { (props) => { return <ConfirmFitness data = { this.state } {...this.props}
                                                        updateState = {this.updateStateHandler}
                                                        backward = {()=>this.updateFormHandler(2)}
                                                        updateForm = {()=>this.updateFormHandler(5)} />}} 
                        />
                        <Route path={this.props.match.url+"/description"} 
                        render = { (props) => { return <AboutYou data = { this.state.description } {...this.props}
                                                        updateState = {this.updateStateHandler}
                                                        backward = {()=>this.updateFormHandler(3)}
                                                        updateForm = {()=>this.updateFormHandler(0)} />}} 
                        />
                    </Switch>
                </section>
            </div>
        );
    }
};

export default SignUpSteps;