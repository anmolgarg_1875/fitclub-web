import React, { Component } from 'react';
import axios from '../../axios';
import ApiURL from '../../services/ApiURL';
import IO from "socket.io-client";

import classes from './Dashboard.css';
import HeartRate from './HeartRate/HeartRate';
import Activity from './Activity/Activity';
import BootstrapModal from '../../components/UI/Modal/BootstrapModal';
import AddActivity from '../../containers/Activity/AddActivity/AddActivity';

const socket = IO('https://dev.api.bambooapp.com/');

class Dashboard extends Component {
    constructor() {
        super();
        this.state = {
            coaches: [],
            activityTypes: [],
            users: [],
            ongoing: [],
            upcoming: [],
            past: [],
            modalStatus: false
        }
    }

    componentDidMount() {
        if (localStorage.getItem("formStep") !== null) {
            this.props.history.push({
                pathname: '/SignUpSteps'
            });
        } else {
            this.getData();
            this.getActivityData();
            this.getCoaches();
            this.getActivityTypes();
        }
    }

    getCoaches = () => {
        const formData = new FormData();
        formData.append('type', 'COACH');
        formData.append('gymId', localStorage.getItem('gymId'));
        axios.post(ApiURL._getUsers, formData).then(response => {
            console.log("response", response);    
            this.setState({
                coaches: response.data.data.map(el => {
                    return { label: el.firstName+' '+el.lastName, value: el._id }      
                })    
            });
        });        
    }

    getActivityTypes = () => {
        axios.post(ApiURL._getActivityTypes).then(response => {
            this.setState({
                activityTypes: response.data.data.map(el => {
                    return { label: el.name, value: el._id }      
                })    
            });
        });        
    }

    // =================  SOCKET EMIT FOR EACH USER ============

    socketEmitHandler = (id) => {
        socket.emit("subscribe",{userId: id});
    };

    // ================ HEART RATE BOX COLOR HANDLER ===========

    colorSelector = ( heartRate ) => {
        let color = "#979797";
        switch(true) {
            case ( heartRate >= 50 && heartRate < 60 ):
                    color = "#507FB1";
                break;
            case ( heartRate >= 60 && heartRate < 70 ):
                    color = "#55BA6B";
                break;
            case ( heartRate >= 70 && heartRate < 80 ):
                    color = "#D6C345";
                break;
            case ( heartRate >= 80 && heartRate < 90 ):
                    color = "#CF832D";
                break;  
            case ( heartRate >= 90 ):
                    color = "#F22B5D";
                break;
            default:
                    color = "#979797";
        }
        return color;
    }

    // ============ UPDATE HEART RATE HANDLER ==========

    socketDataHandler = (obj) => {
        let age;
        const index = this.state.users.findIndex(el => obj.userId === el._id);
        if (this.state.users[index].dob && this.state.users[index].dob.year) {
            // age = 2020 - parseInt(this.state.users[index].dob.year);
            age = 2020 - this.state.users[index].dob.year;
        }
        const heartRatePercentage = (obj.heartRate*100)/(220-age);
        this.setState(prevState =>({
                users:  prevState.users.map( el =>
                        el._id === obj.userId ? {
                             ...el,
                              heartRate: obj.heartRate,
                              heartRatePercentage: heartRatePercentage.toFixed(0),
                            //   color: this.colorSelector(parseInt(heartRatePercentage.toFixed(0)))                             
                              color: this.colorSelector(heartRatePercentage.toFixed(0))                             
                        } : el 
                )         
            }));
        }

    getActivityData = () => {
        const formData = new FormData();       
        formData.append('skip', '0');
        formData.append('limit', '10');
        formData.append('past', 'true');
        formData.append('ongoing', 'true');
        formData.append('upcoming', 'true');
        formData.append('gymId', localStorage.getItem('gymId'));
        axios.post(ApiURL._getActivity, formData).then(response => {
            console.log("getActivity", response);
            this.setState({
                ongoing: [...response.data.data.ongoing],
                past: [...response.data.data.past],
                upcoming: [...response.data.data.upcoming]
            })
        });
    }

    // ============= GET ALL USERS  ======================

    getData = () => {
        const formData = new FormData();
        formData.append('type','MEMBER');
        formData.append('gymId', localStorage.getItem('gymId'));
        axios.post(ApiURL._getUsers, formData).then(response => {
            response.data.data.forEach(element => {
                element['heartRate'] = "0";
                element['color'] = '#979797';
                element['heartRatePercentage'] = "0";
                this.socketEmitHandler(element._id);
            });
            //  ====   SOCKET LISTENER  ====
            socket.on("heartbeat", this.socketDataHandler);
                // =======================
            this.setState({
                users: [...response.data.data]
            })
        });    
    }

    modalClose = ()=> {
        this.setState({
            modalStatus: false    
        });        
    }

    render() {
        // =====UPCOMING ACTIVITIES=======       
        let upcomingActivityArray = null;
        !this.state.upcoming.length?
        upcomingActivityArray = (<h1>No activity yet!</h1>) :                
        upcomingActivityArray = this.state.upcoming.map(el => 
            <Activity key = {el._id} data = {el}/>);
        // =====ONGOING ACTIVITIES=======       
        let ongoingActivityArray = null;
        !this.state.ongoing.length?
        ongoingActivityArray = (<h1>No activity yet!</h1>) :                
        ongoingActivityArray = this.state.ongoing.map(el => 
            <Activity key = {el._id} data = {el}/>);
        // =====PAST ACTIVITIES=======       
        let pastActivityArray = null;
        !this.state.past.length?
        pastActivityArray = (<h1>No activity yet!</h1>) :                
        pastActivityArray = this.state.past.map(el => 
            <Activity key = {el._id} data = {el}/>);
        // =====USERS HEARTRATE=======       
        let heartRateItemsArray = null;
        !this.state.users.length ?
            heartRateItemsArray = (<h1>Loading .......</h1>) :
                heartRateItemsArray = this.state.users.map( userData=> 
                <HeartRate key={userData._id}  data={userData} /> )
        // ==========================           
        return (
            <section className = {classes.main_div}>
                <div className = {classes.container}>    
                    <div className = {classes.parent_div}>
                        <div className = {classes.activity_box}>
                            <div className={classes.activity_header}>
                                <h1>Activities</h1>
                                <button className = {classes.add_activity} onClick={() => this.setState({modalStatus: true})}>Add Activity</button>
                            </div>

                            <h1 className = {classes.activity_title}>Ongoing</h1>
                            <div className={classes.activity_box_type}>
                                {ongoingActivityArray}
                            </div>

                            <h1 className = {classes.activity_title}>Upcoming</h1>
                            <div className={classes.activity_box_type}>
                                {upcomingActivityArray}   
                            </div>
                            
                            <h1 className = {classes.activity_title}>Past</h1>
                            <div className={classes.activity_box_type}>
                                {pastActivityArray}     
                            </div>
                        </div>
                        <div className = {classes.heart_box}>
                            {heartRateItemsArray}       
                        </div>
                    </div>
                </div>
                <BootstrapModal show={this.state.modalStatus} onHide={this.modalClose} title="Add Activity">
                    <AddActivity updateActivity = {this.getActivityData} activityTypes = {this.state.activityTypes} coaches = {this.state.coaches}  onHide={this.modalClose} />        
                </BootstrapModal>
            </section>
        );
    }
}

export default Dashboard;