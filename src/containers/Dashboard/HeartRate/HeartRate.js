import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart,faFireAlt,faHeartbeat } from '@fortawesome/free-solid-svg-icons'

import classes from './HeartRate.css';
import userDummy from '../../../assets/Images/userDummy.png';

const heartRate = (props) => {
    return(
            <div className = {classes.heart_box_item} style={{backgroundColor: props.data.color}}>        
                <div className = {classes.user_info}>       
                    <img src = {(props.data.profilePic)?props.data.profilePic.thumb:userDummy} alt="sdsd"/>
                    <p>{props.data.firstName}</p>
                </div>
                <div className = {classes.user_workout}>
                    <ul>
                        <li><FontAwesomeIcon icon = { faHeartbeat } /></li>
                        <li>{props.data.heartRatePercentage} %</li>
                    </ul>
                    <ul>
                        <li><FontAwesomeIcon icon = { faHeart } /></li>
                        <li>{props.data.heartRate} </li>
                    </ul>
                    <ul>
                        <li><FontAwesomeIcon icon = { faFireAlt } /></li>
                        <li>--</li>
                    </ul>
                    <p>Rank--</p>
                </div>
            </div>
    );   
}

export default heartRate;

