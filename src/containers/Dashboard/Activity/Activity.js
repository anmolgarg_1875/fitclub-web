import React from 'react';
import moment from 'moment';

import classes from './Activity.css'
import userDummy from '../../../assets/Images/userDummy.png';

const Activity = (props) => {

    let color = {borderColor: "#55BA6B"};

    switch ( true ) {
        case ( props.data.intensityLevel === "OPEN" ) :
                // ==========GREEN=============
                color = {borderColor: "#55BA6B"};
            break;
        case ( props.data.intensityLevel === "INTERMEDIATE" ) :
                // ==========ORANGE============
                color = {borderColor: "#FF7B2A"};
            break;
        case ( props.data.intensityLevel === "ADVANCE" ) :
                // ==========MAGENTA===========
                color = {borderColor: "#FF0040"};
            break;
        default:
                // ==========GREEN=============
                color = {borderColor: "#55BA6B"};
    }

    return(
        <div className={classes.activity_box_item} style={color}>        
            <div className={classes.activity_icon}>       
                <img src={props.data.typeId && props.data.typeId.icon.thumb? 
                props.data.typeId.icon.thumb: userDummy} alt = "activityIcon" />
            </div>
            <div className={classes.activity_detail}>
                <h4>{props.data.name}</h4>    
                <p>GOLD Gym, {props.data.coachId.firstName+' '+props.data.coachId.lastName}</p>    
                <p>{moment(props.data.startTime).format('ddd,MMM Do')},{'   '+props.data.duration} Min</p>    
            </div>
        </div>
    );
}

export default Activity;