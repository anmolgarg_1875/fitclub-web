import React, {Component} from 'react';
import { FormBuilder, FieldGroup, FieldControl, Validators } from "react-reactive-form";
import axios from '../../axios';
import ApiURL from '../../services/ApiURL';

 import classes from './Login.css';
 import peopleImage from '../../../src/assets/Images/people.jpeg';
 import TextInput from '../../components/UI/FormInput/FormInput';


class Login extends Component {

    loginForm = FormBuilder.group({
        email : ["", [Validators.required,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$')]],
        password : ["", [Validators.required,Validators.minLength(10),Validators.maxLength(50)]]
    });

    // ===============form submit handler===============

    handleSubmit=(e) => {
        e.preventDefault();
        if (this.loginForm.valid) {
            axios.post(ApiURL._login, this.loginForm.value).then(response => {
                localStorage.setItem('accessToken', response.data.data.accessToken);
                if (response.data.data.caochProfileStep < 10) {
                    // ==========Redirect to signup steps========    
                    localStorage.setItem("formStep", 1);
                    this.props.history.push({
                        pathname: '/SignUpSteps'
                    });
                } else {
                    // ==========Redirect to dashboard===========   
                    if (response.data.data.gymsLinked && response.data.data.gymsLinked.length) {
                        localStorage.setItem('gymId', response.data.data.gymsLinked[0].id);                                
                    }
                    this.props.history.push({pathname: '/'});
                }
            });
        }        
    }

    render() {
        return (
            <section>
                <div className = {classes.banner_image}>    
                </div>
                <div className={classes.login_container}>    
                    <div className={classes.login_gym_image}>         
                    <img src = {peopleImage} alt="people"/>         
                    </div>
                    <div className={classes.login_form}>
                        <h1>Already got an account? Login below</h1>
                        <FieldGroup control={this.loginForm}
                            render={({ get, invalid }) => (
                                <form onSubmit={this.handleSubmit}>
                                    <FieldControl name="email" render={TextInput} meta={{ label: "Email", type: "text" }} />
                                    <FieldControl name="password" render={TextInput}
                                     meta={{ label: "Password", type: "password", minLength: '10', maxLength:'50' }} />
                                    <button type="submit"  className={invalid?classes.Disabled_Button: classes.Enabled_Button}
                                     disabled={invalid} >Submit</button>
                                </form>
                             )}
                        />
                        <a>Forgot your password?</a>
                    </div>
                </div>

            </section>
        );
    }
};

export default Login;