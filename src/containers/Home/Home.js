import React, {Component} from 'react';
import {Link} from 'react-router-dom';


import classes from './Home.css';
import gymImage from '../../../src/assets/Images/gym.jpg';



class Home extends Component {

    render() {
        return (
            // ======================= Fitclub Welcome Screen =====================

            <section>
                <div className = {classes.banner_image}>    
                </div>
                <div className = {classes.login_container}>    
                    <div className = {classes.login_gym_image}>         
                        <img src = {gymImage}  alt="people"/>      
                    </div>
                    <div className = {classes.login_form}>
                        <Link to = "/Login"><button className = {classes.Login}>Login</button></Link>        
                        <Link to = "/SignUp"><button className = {classes.Signup}>Sign Up</button></Link>        
                        {/* <button className = {classes.Facebook}>Facebook</button>         */}
                        <div className = {classes.policy_div}>
                            <a>Terms And Conditions</a>
                            <a>Privacy Policy</a>    
                        </div>
                    </div>
                </div>
            </section>

        );
    }
};

export default Home;